# jira-get-stakeholders

Create config.txt in this directory as follows

```
[BASICAUTH]
JIRA_USER = yourjirauser
JIRA_PASS = yourjirapassword

[DETAILS]
JIRA_URL = https://jira.domain.tld
PROJECT = PROJECTNAME
