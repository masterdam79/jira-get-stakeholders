#!/usr/bin/env python3

import configparser
from jira import JIRA
from jira.client import GreenHopper
import sys
import argparse
from pprint import pprint

# Get some variables outside this script
config = configparser.ConfigParser()
config.read('./config.txt')
jira_user = config['BASICAUTH']['JIRA_USER']
jira_pass = config['BASICAUTH']['JIRA_PASS']
jira_url = config['DETAILS']['JIRA_URL']
jira_project = config['DETAILS']['PROJECT']

parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--sprint', type=str)
parser.add_argument('--verbose', type=str)
args = parser.parse_args()

if args.verbose != None: print("Connect to JIRA with Basic Auth")
jira = JIRA(basic_auth=(jira_user, jira_pass), options = {'server': jira_url})
if args.verbose != None: print("Query JIRA to iterate over issues in sprint " + str(args.sprint))
for issue in jira.search_issues('project = ' + str(jira_project) + ' AND Sprint = "CP Sprint ' + str(args.sprint) + '" AND status in ("Ready for test", "Ready to release", Open, "Development in Progress")', maxResults=0):
    if args.verbose != None: print("Processing " + str(issue))
    if args.verbose != None: print("Adding reporters to watchers")
    jira.add_watcher(issue.key, issue.fields.reporter.key)
    if args.verbose != None: print("Getting watchers")
    watchers = jira.watchers(issue.key)
    if args.verbose != None: print("Iterating over watchers")
    for watcher in watchers.watchers:
        if args.verbose != None: print("Processing watcher " + watcher.emailAddress)
        print(jira_url + "/browse/" + issue.key + " " +  watcher.emailAddress + " " +  watcher.displayName)
