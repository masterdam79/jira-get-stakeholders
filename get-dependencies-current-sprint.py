#!/usr/bin/env python3

import configparser
from jira import JIRA
from jira.client import GreenHopper
import sys
from pprint import pprint

# Get some variables outside this script
config = configparser.ConfigParser()
config.read('./config.txt')
jira_user = config['BASICAUTH']['JIRA_USER']
jira_pass = config['BASICAUTH']['JIRA_PASS']
jira_url = config['DETAILS']['JIRA_URL']
jira_project = config['DETAILS']['PROJECT']

jira = JIRA(basic_auth=(jira_user, jira_pass), options = {'server': jira_url})

for issue in jira.search_issues('project = ' + jira_project + ' and Sprint in openSprints()', maxResults=0):
    links = jira.remote_links(issue)
    pprint(issue)
    pprint(links)
#    for link in links.watchers:
#        print(jira_url + "/browse/" + issue.key + " " +  watcher.emailAddress + " " +  watcher.displayName)
